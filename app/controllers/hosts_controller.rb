class HostsController < ApplicationController

  def new_host
    @hosts = Host.all.order('id desc').pluck(:name)
  end

  def store_hosts
    hosts = params[:hosts]
    data = Host.bulk_store(hosts)

    render json: {hosts: data}
  end

end
