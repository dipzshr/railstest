class Host < ActiveRecord::Base

  def self.bulk_store(host_names)
    data = {}
    host_names.each do |key, value|
      if self.create(name: value)
        data[key] = value
      end
    end
    data
  end

end
