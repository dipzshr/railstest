$(document).on 'ready page:load', ->
  i = 0
  hosts = {}

  setInterval (->
    storeHosts(hosts) unless jQuery.isEmptyObject(hosts)
    return
  ), 15000

  $('#host-name').on 'keypress', (e) ->
    code = e.keyCode || e.which
    if code==13 && val = $(this).val()
      $(this).val('')
      string = parseHostName(val)
      if isHostName(string)
        renderHost(string)

  renderHost = (validHostName) ->
    $('.lists').prepend("<p>#{validHostName}</p>")
    maintainData(validHostName)

  isHostName = (string) ->
    if (/^((2[0-4]|1[0-9]|[1-9])?[0-9]|25[0-5])(\.((2[0-4]|1[0-9]|[1-9])?[0-9]|25[0-5])){3}$/.test(string))
      octets = string.split('.')
      if ((octets[0] == 0) || (octets[0] == 10) || (octets[0] == 127) || (octets[3] == 255) || (octets[3] == 0) || ((octets[0] == 169) && (octets[1] == 254)) || ((octets[0] == 172) && (octets[1] & 0xf0 == 16)) || ((octets[0] == 192) && (octets[1] == 0) && (octets[2] == 2))|| ((octets[0] == 192) && (octets[1] == 88) && (octets[3] == 99))|| ((octets[0] == 192) && (octets[1] == 168)) || ((octets[0] == 198) && (octets[1] & 0xfe == 18)) || (octets[0] & 0xf0 == 224) || (octets[0] & 0xf0 == 240))
        return false
    else if (!/^([a-z0-9][a-z0-9-]*[a-z0-9]\.)+[a-z]{2,}$/.test(string))
      return false
    return true
  
  parseHostName = (string) ->
    ret = string.replace(/.*?:\/\//g, "")
    return ret

  maintainData = (hostName) ->
    i += 1
    name = 'name' + i
    hosts[name] = hostName

  storeHosts = ->
    $.ajax
      type: 'PUT'
      dataType: 'json'
      data: {hosts: hosts} 
      url: '/hosts/store_hosts'
      success: (response) ->
        for host_name of response.hosts
          delete hosts[host_name]
    return

